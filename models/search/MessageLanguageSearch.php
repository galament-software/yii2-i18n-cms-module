<?php

namespace bariew\i18nModule\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use bariew\i18nModule\models\MessageLanguage;

/**
 * MessageLanguageSearch represents the model behind the search form about `bariew\i18nModule\models\MessageLanguage`.
 */
class MessageLanguageSearch extends MessageLanguage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MessageLanguage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
